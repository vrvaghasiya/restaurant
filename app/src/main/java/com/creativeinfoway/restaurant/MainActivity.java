package com.creativeinfoway.restaurant;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends Activity implements View.OnClickListener {

    ImageView imgtableleftfloor, imgtablerightfloor;
    LinearLayout linearLayoutright;
    RelativeLayout relativeLayout, lineralayoutparent;
    Button btnNext;
    String tableno;
    int tableOutlineColor;
    int tableBgColor;
    String s_progress, s_status;
    TextView tvProgress, tvStatus, tvTableNO;
    ArrayList<FloorView> viewArrayList = new ArrayList<>();
    FloorView floorView;

    float xx, yy;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // getSupportActionBar().hide();
        initid();
    }

    private void initid() {
        linearLayoutright = findViewById(R.id.ll_right);
        imgtableleftfloor = findViewById(R.id.img_table_floor_left);
        imgtablerightfloor = findViewById(R.id.img_ll_right);
        lineralayoutparent = findViewById(R.id.parent);
        relativeLayout = findViewById(R.id.parent_relative);
        tvProgress = findViewById(R.id.tv_progress);
        tvStatus = findViewById(R.id.tv_status);
        tvTableNO = findViewById(R.id.tv_table_no);
        btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);
        imgtableleftfloor.setOnClickListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        if (v == imgtableleftfloor) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_input, null);
            dialogBuilder.setView(dialogView);

            final EditText edt = (EditText) dialogView.findViewById(R.id.et_table_no);
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @SuppressLint("ClickableViewAccessibility")
                public void onClick(DialogInterface dialog, int whichButton) {

                    if (!edt.getText().toString().isEmpty()) {

                        tableno = edt.getText().toString();

                        String[] tablenoarray = tableno.split(",");

                        for (int i = 0; i < tablenoarray.length; i++) {
                            String name = tablenoarray[i];
                            final int min = 1;
                            final int max = 5;
                            final int random = new Random().nextInt((max - min) + 1) + min;
                            s_progress = String.valueOf(random);

                            tableOutlineColor = R.color.black;
                            tableBgColor = R.color.white;
                            s_status = "pending";

                            floorView = new FloorView(getApplicationContext(), name);
                            floorView.setY(9);
                            floorView.setX(190);
                            lineralayoutparent.addView(floorView);
                            viewArrayList.add(floorView);
                            floorView.setOnTouchListener(onTouchListener());

                        }


                    } else {
                        dialog.dismiss();
                    }

                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }
        if (v == btnNext) {

            ArrayList<JSONObject> floorArrayList = new ArrayList<>();
            for (int i = 0; i < viewArrayList.size(); i++) {

                FloorView floorView = viewArrayList.get(i);
                floorArrayList.add(floorView.toJson());
            }

            Intent intent = new Intent(this, TableDetailsActivity.class);
            intent.putExtra("floorlist", floorArrayList.toString());
            startActivity(intent);

        }
    }



    private View.OnTouchListener onTouchListener() {
        return new View.OnTouchListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @SuppressLint("ClickableViewAccessibility")
            @Override

            public boolean onTouch(View view, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        xx = view.getX() - event.getRawX();
                        yy = view.getY() - event.getRawY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        view.animate()
                                .x(event.getRawX() + xx)
                                .y(event.getRawY() + yy)
                                .setDuration(0)
                                .start();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        };
    }
}


   /* @SuppressLint("ClickableViewAccessibility")
    private void setmultipleview(int i1) {

        floorView = new FloorView(this, String.valueOf(i1));
        floorView.setY(9);
        floorView.setX(190);
        lineralayoutparent.addView(floorView);
        viewArrayList.add(floorView);
        floorView.setOnTouchListener(onTouchListener());
    }*/


//    int[] colorarray1 = new int[]{
//            (R.color.NoOrder), (R.color.newOrder), (R.color.progress1), (R.color.progress2), (R.color.finish), (R.color.disable)
//    };
//
//    String[] statusarray = new String[]{
//            "MERGE",
//            "CLOSED",
//            "PAUSED ",
//            "OCCUPIED ",
//            "RESERVED"
//    };


//                int randomAndroidColor = colorarray1[new Random().nextInt(colorarray1.length)];
//                String randomstatus = statusarray[new Random().nextInt(statusarray.length)];
//                final int min = 1;
//                final int max = 5;
//                final int random = new Random().nextInt((max - min) + 1) + min;}

//
//                int[] location = new int[2];
//                floorView.getLocationInWindow(location);
//                int x = location[0];
//                int y = location[1];


//                JSONObject jsonObject=new JSONObject();
//                try {
//                    jsonObject.put("x",floorView.xDelta);
//                    jsonObject.put("y",floorView.yDelta);
//                    jsonObject.put("tableno",floorView.tv_table_no.getText());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                Floor floor = new Floor(floorView.tv_table_no.getText().toString(), R.color.black, randomAndroidColor, random, randomstatus, floorView.getX(), floorView.getY());