package com.creativeinfoway.restaurant;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Random;

public class FloorView extends LinearLayout implements Serializable {
    TextView tv_table_no;
    TextView tvProgress;
    ImageView imageView, imageView1;
    LinearLayout linearLayout;
    ProgressBar progressBar;
    int xDelta, yDelta;

    private static final String TABLE_NO="tableNo";
    private static final String TABLE_OUTLINE_COLOR="tableOutlineColor";
    private static final String TABLE_BG_COLOR="tableBgColor";
    private static final String PROGRESS="progress";
    private static final String STATUS="status";
    private static final  String TABLE_POSITION_X="table_position_x";
    private static final String TABLE_POSITION_Y="table_position_y";


    private int[] colorarray = new int[]{
            (R.color.NoOrder), (R.color.newOrder), (R.color.progress1), (R.color.progress2), (R.color.finish), (R.color.disable)
    };

    private String[] statusarray = new String[]{
            "MERGE",
            "CLOSED",
            "PAUSED ",
            "OCCUPIED ",
            "RESERVED"
    };


    public FloorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }

    public FloorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public FloorView(Context context, JSONObject jsonObject) {
        super(context);
        initView();
        try {

            imageView.setVisibility(VISIBLE);
            progressBar.setVisibility(VISIBLE);
            tvProgress.setVisibility(VISIBLE);

            imageView1.setBackgroundResource(Integer.parseInt(jsonObject.getString(TABLE_BG_COLOR)));


            this.setX(Float.parseFloat(jsonObject.getString(TABLE_POSITION_X)));
            this.setY(Float.parseFloat(jsonObject.getString(TABLE_POSITION_Y)));

            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
            this.tv_table_no.setText(String.valueOf(jsonObject.getString(TABLE_NO)));

            String progress = String.valueOf(jsonObject.getString(PROGRESS));

            this.tvProgress.setText(String.valueOf(progress + "/5"));

            int sp = Integer.parseInt(progress);

            for (int i = 1; i <= sp; i++) {
                progressBar.setProgress(progressBar.getProgress() + 20);
            }

            switch (jsonObject.getString(STATUS)) {
                case "MERGE":

                    imageView.setImageResource(R.drawable.noti_merge);
                    break;
                case "CLOSED":
                    imageView.setImageResource(R.drawable.noti_closed);
                    break;
                case "PAUSED":
                    imageView.setImageResource(R.drawable.noti_pause);
                    break;
                case "OCCUPIED":
                    imageView.setImageResource(R.drawable.noti_play);
                    break;
                case "RESERVED":
                    imageView.setImageResource(R.drawable.noti_reserved);
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public FloorView(Context context, String num) {
        super(context);
        initView();
        tv_table_no.setText(num);
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject();
        final int min = 1;
        final int max = 5;
        final int random = new Random().nextInt((max - min) + 1) + min;

        int randomAndroidColor = colorarray[new Random().nextInt(colorarray.length)];
        String randomstatus = statusarray[new Random().nextInt(statusarray.length)];

        try {
            jsonObject.put(TABLE_NO, tv_table_no.getText());
            jsonObject.put(TABLE_OUTLINE_COLOR, R.color.black);
            jsonObject.put(TABLE_BG_COLOR, randomAndroidColor);
            jsonObject.put(PROGRESS, random);
            jsonObject.put(STATUS, randomstatus);
            jsonObject.put(TABLE_POSITION_X, this.getX());
            jsonObject.put(TABLE_POSITION_Y, this.getY());
        } catch (JSONException e) {
        }
        return jsonObject;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.onInterceptTouchEvent(ev);
    }

    public void updateExisting(JSONObject jsonObject) {
        try {

            imageView.setVisibility(VISIBLE);
            progressBar.setVisibility(VISIBLE);
            tvProgress.setVisibility(VISIBLE);

            imageView1.setBackgroundResource(Integer.parseInt(jsonObject.getString(TABLE_BG_COLOR)));


            this.setX(Float.parseFloat(jsonObject.getString(TABLE_POSITION_X)));
            this.setY(Float.parseFloat(jsonObject.getString(TABLE_POSITION_Y)));

            linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
            this.tv_table_no.setText(String.valueOf(jsonObject.getString(TABLE_NO)));

            String progress = String.valueOf(jsonObject.getString(PROGRESS));

            this.tvProgress.setText(String.valueOf(progress + "/5"));

            int sp = Integer.parseInt(progress);

            for (int i = 1; i <= sp; i++) {
                progressBar.setProgress(progressBar.getProgress() + 20);
            }

            switch (jsonObject.getString("status")) {
                case "MERGE":

                    imageView.setImageResource(R.drawable.noti_merge);
                    break;
                case "CLOSED":
                    imageView.setImageResource(R.drawable.noti_closed);
                    break;
                case "PAUSED":
                    imageView.setImageResource(R.drawable.noti_pause);
                    break;
                case "OCCUPIED":
                    imageView.setImageResource(R.drawable.noti_play);
                    break;
                case "RESERVED":
                    imageView.setImageResource(R.drawable.noti_reserved);
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initView() {
        View view = inflate(getContext(), R.layout.floorview, null);
        tv_table_no = view.findViewById(R.id.tv_table_nocustom);
        tvProgress = view.findViewById(R.id.flor_tv_progress);
        imageView = view.findViewById(R.id.img_status);
        linearLayout = view.findViewById(R.id.florviewll_right);
        progressBar = view.findViewById(R.id.floorprogress);
        imageView1 = view.findViewById(R.id.img_ll_right);

        addView(view);
    }

    private View.OnTouchListener onTouchListener() {
        return new View.OnTouchListener() {

            int x, y;

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(final View view, MotionEvent event) {

                x = (int) event.getRawX();
                y = (int) event.getRawY();


                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN:
                        view.getParent().requestDisallowInterceptTouchEvent(true);
                        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
                        xDelta = x - layoutParams.leftMargin;
                        yDelta = y - layoutParams.topMargin;

                        break;

                    case MotionEvent.ACTION_UP:

                        break;

                    case MotionEvent.ACTION_MOVE:
                        LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) view.getLayoutParams();
                        layoutParams1.leftMargin = x - xDelta;
                        layoutParams1.topMargin = y - yDelta;
                        layoutParams1.rightMargin = -250;
                        layoutParams1.bottomMargin = -250;
                        view.setLayoutParams(layoutParams1);


                        break;
                }
                linearLayout.invalidate();
                return true;
            }
        };
    }



}
