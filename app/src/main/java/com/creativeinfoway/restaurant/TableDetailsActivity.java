package com.creativeinfoway.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TableDetailsActivity extends Activity implements View.OnClickListener {
    private static final String TABLE_NO = "tableNo";
    private static final String TABLE_OUTLINE_COLOR = "tableOutlineColor";
    private static final String TABLE_BG_COLOR = "tableBgColor";
    private static final String PROGRESS = "progress";
    private static final String STATUS = "status";
    private static final String TABLE_POSITION_X = "table_position_x";
    private static final String TABLE_POSITION_Y = "table_position_y";
    ImageView imgtablerightfloor;
    LinearLayout linearLayoutright;
    RelativeLayout relativeLayout;
    Button btncreate;
    TextView tvProgress, tvTableNO;
    ProgressBar progressBar;
    ArrayList<FloorView> floorViewArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_details);
        initid();
        parseintentdata();
    }

    private void initid() {
        relativeLayout = findViewById(R.id.act_tb_main_relative);
        btncreate = findViewById(R.id.btn_create);
        progressBar = findViewById(R.id.act_tb_progressbar);
        linearLayoutright = findViewById(R.id.act_tb_ll_right);
        imgtablerightfloor = findViewById(R.id.act_tb_img_ll_right);
        tvProgress = findViewById(R.id.act_tb_tv_progress);
        tvTableNO = findViewById(R.id.act_tb_tv_table_no);
        btncreate.setOnClickListener(this);
    }

    private void parseintentdata() {


        Intent intent = getIntent();
        String jsonArray = intent.getStringExtra("floorlist");

        try {
            JSONArray array = new JSONArray(jsonArray);
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                FloorView floorView = new FloorView(getApplicationContext(), jsonObject);
                relativeLayout.addView(floorView);
                floorViewArrayList.add(floorView);
                floorView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Toast.makeText(TableDetailsActivity.this, jsonObject.getString("tableNo"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {

        if (v == btncreate) {

           // updateExistingTable(new JSONObject());
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }


    // Update tabledata from existing tabledata

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateExistingTable(final JSONObject jsonObject) {

        for (FloorView floorView : floorViewArrayList) {
            try {
                if (floorView.tv_table_no.getText().toString().equals(jsonObject.getString("tableNo"))) {
                    floorView.updateExisting(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

}



